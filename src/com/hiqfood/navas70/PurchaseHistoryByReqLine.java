package com.hiqfood.navas70;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hiqfood.navws70.ODataServices;

/**
 * Servlet implementation class PurchaseHistoryByReqLine
 */
@WebServlet("/PurchaseHistoryByReqLine")
public class PurchaseHistoryByReqLine extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PurchaseHistoryByReqLine() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("json/application");
		PrintWriter out = response.getWriter();
		URL url = new URL("http://nav2013.hiqfood.com:7048/DynamicsNAV70/OData/Company('HIQFOOD')/PurchaseHistoryByReqLine");
		out.print(ODataServices.getJSON(url));
	}

}
