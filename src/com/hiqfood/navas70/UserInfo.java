package com.hiqfood.navas70;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.hiqfood.navws70.Authenticator;

/**
 * Servlet implementation class UserInfo
 */
@WebServlet(description = "Get info of the login user.", urlPatterns = { "/UserInfo" })
public class UserInfo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfo() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Authenticator auth = new Authenticator(request, response);
        String authInfo = auth.getAuthInfo(true);
        if (authInfo == null) return;
		
		try {
			String userName = null;
			JSONObject result = new JSONObject();
			JSONArray ja = new JSONArray(authInfo);
			for (int i=0; i<ja.length(); i++) {
				JSONObject jo = ja.getJSONObject(i);
				if (jo.get("type").equals("User")) {
					userName = (String) jo.get("name");
				}
			}
			if (userName != null) {
				JSONObject joUser = new JSONObject();
				joUser.put("name", userName);
				result = joUser;
			}
	        response.setContentType("json/application; charset=UTF-8");
			//response.setContentType("text/html; charset=UTF-8");
			response.setCharacterEncoding("UTF-8");
			PrintWriter out = response.getWriter();
			out.print(result);
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
}