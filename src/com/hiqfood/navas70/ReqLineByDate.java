package com.hiqfood.navas70;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.hiqfood.navws70.PurchaseUtil;

/**
 * Servlet implementation class ReqLineByDate
 */
@WebServlet(description = "Purchase requisition worksheet line by date", urlPatterns = { "/ReqLineByDate" })
public class ReqLineByDate extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ReqLineByDate() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String journalBatchName = request.getParameter("journalBatch");
		response.setContentType("json/application; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(PurchaseUtil.getRequisitionLines(journalBatchName));
	}
}