package com.hiqfood.navas70;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.hiqfood.navws70.SoapServices;

/**
 * Servlet implementation class PQNos
 */
@WebServlet("/PQNos")
public class PQNos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public PQNos() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html; charset=UTF-8");
		response.setCharacterEncoding("UTF-8");
		PrintWriter out = response.getWriter();
		out.print(new Gson().toJson(SoapServices.getPurchaseQuoteNos()));
	}
}
