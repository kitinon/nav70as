/*
 * File: app/model/PurchaseHistoryByReqLineModel.js
 *
 * This file was generated by Sencha Architect version 2.2.2.
 * http://www.sencha.com/products/architect/
 *
 * This file requires use of the Ext JS 4.2.x library, under independent license.
 * License of Sencha Architect does not include license for Ext JS 4.2.x. For more
 * details see http://www.sencha.com/license or contact license@sencha.com.
 *
 * This file will be auto-generated each and everytime you save your project.
 *
 * Do NOT hand edit this file.
 */

Ext.define('AutoVendor.model.PurchaseHistoryByReqLineModel', {
    extend: 'Ext.data.Model',

    fields: [
        {
            convert: function(v, rec) {
                return new Date(v.match(/\d+/)[0] * 1);
            },
            name: 'Due_Date',
            type: 'date'
        },
        {
            name: 'No'
        },
        {
            name: 'Description'
        },
        {
            name: 'Quantity',
            type: 'float'
        },
        {
            name: 'Unit_of_Measure_Code'
        },
        {
            name: 'Buy_from_Vendor_No'
        },
        {
            name: 'Min_Direct_Unit_Cost',
            type: 'float'
        },
        {
            name: 'Max_Direct_Unit_Cost',
            type: 'float'
        }
    ]
});